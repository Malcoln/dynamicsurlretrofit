package com.example.dynamicsurlretrofit

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.experimental.CoroutineCallAdapterFactory
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/*******
 * /** Either "http" or "https". */
final String scheme;

/** Decoded username. */
private final String username;

/** Decoded password. */
private final String password;

/** Canonical hostname. */
final String host;

/** Either 80, 443 or a user-specified port. In range [1..65535]. */
final int port;
 */

object ApiModule {

    private var scheme: String = ""
    set(url){
        field = HttpUrl.parse(url)!!.scheme()
    }

    private var host: String =""
    set(url){
        field = HttpUrl.parse(url)!!.host()
    }

    fun provideOkHttpClient() = OkHttpClient.Builder()
        .addInterceptor {
            val request = it.request()
            val newUrl: HttpUrl?
            newUrl = when {
                scheme != null && host != null ->
                    request.url().newBuilder()
                        .scheme(scheme)
                        .host(host)
                        .addQueryParameter("apikey", "something")
                        .build()
            }
            it.proceed(
                request.newBuilder()
                    .url(newUrl)
                    .build()
            )

        }
        .build()!!

    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit.Builder{

        val okHttpBuilder =  okHttpClient.newBuilder()
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        //também é possível definir level de camada HEADER
        okHttpBuilder.addInterceptor(httpLoggingInterceptor)

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .client(okHttpBuilder.build())
            .baseUrl("http://localhost")
    }
}